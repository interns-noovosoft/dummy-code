<?php

namespace App\Http\Controllers\Api;

use App\Domain\Compartment\Compartment;
use App\Domain\Prescription\Prescription;
use App\Domain\Compartment\CompartmentPrescription;
use App\Domain\Reminder\Reminder;
use App\Domain\Reminder\ReminderCompartment;
use App\Domain\User\UserContact;
use App\Domain\User\UserDmrSetting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DmrController extends Controller
{
    public function postFillDmr($request)
    {
//        request()->validate([             //TODO: Only for testing. Restore later
//            'fill_compartments' => 'required|array|min:1',
//        ]);

        $user = request()->user();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        $compartments = Compartment::query()->where('user_id', '=', $userId)->get();
        $prescriptions = Prescription::query()->where('user_id', '=', $userId)->get();

        $uniqueReminders = [];
        foreach ($request['fill_compartments'] as $key => $fillCompartment) {
            foreach ($fillCompartment['medications'] as $medication) {
                $compartmentPrescription = new CompartmentPrescription();
                $compartmentPrescription->user_id = $userId;
                $compartmentPrescription->compartment_id = $fillCompartment['compartment']['id'];
                $compartmentPrescription->prescription_id = $medication['prescription_id'];
                $compartmentPrescription->get();

                $compartment = $compartments->where('id', '=', $fillCompartment['compartment']['id'])->first();
                $compartment->is_empty = 0;
                $compartment->save();

                $prescription = $prescriptions->where('id', '=', $medication['prescription_id'])->first();
                $prescription->is_in_dmr = 1;
                $prescription->in_dmr += 1;
                $prescription->save();
            }
            $uniqueReminders[$fillCompartment['schedule']][$key] = $fillCompartment;
        }

        foreach ($uniqueReminders as $schedule => $uniqueReminder) {
            $reminder = new Reminder();
            $reminder->user_id = 2;
            $reminder->reminder_date_time = $schedule;
            $reminder->save();

            foreach ($uniqueReminder as $compartmentReminder) {
                $reminderCompartment = new ReminderCompartment();
                $reminderCompartment->user_id = 2;
                $reminderCompartment->compartment_id = $compartmentReminder['compartment']['id'];
                $reminderCompartment->reminder_id = $reminder->id;
                $reminderCompartment->save();
            }
        }
    }

    public function getUserSettings()
    {
        $user = request()->userId();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        $userDmrSettings = UserDmrSetting::query()
            ->where('user_id', '=', $userId)
            ->get();

        $userContacts = UserContact::query()
            ->where('user_id', '=', $userId)
            ->get();

        $response['settings'] = $userDmrSettings->toArray();
        $response['primary_contacts'] = $userContacts->where('is_primary', '=', 1)->toArray();

        return $response;
    }

    public function postUserSettings(Request $request)
    {
        request()->validate([
            'time_zone' => 'nullable|string|max:255',
            'is_automatic_time_zone' => 'nullable|bool',
            'is_24_hours' => 'nullable|bool',
            'is_restricted_mode' => 'nullable|bool',
            'is_snooze_allowed' => 'nullable|bool',
            'max_snooze_mins' => 'nullable|integer|min:1',
        ]);

        $user = request()->user();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        $userDmrSettings = UserDmrSetting::query()
            ->where('user_id', '=', $userId)
            ->first();

        foreach ($request->toArray() as $setting => $value) {
            $userDmrSettings->$setting = $value;
        }

        $userDmrSettings->save();
        return $userDmrSettings;
    }
}
