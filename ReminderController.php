<?php

namespace App\Http\Controllers\Api;

use App\Domain\Compartment\Compartment;
use App\Domain\Compartment\CompartmentPrescription;
use App\Domain\Prescription\Prescription;
use App\Domain\Reminder\Reminder;
use App\Domain\Reminder\ReminderCompartment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReminderController extends Controller
{
    public function getReminder(Request $request)
    {
        request()->validate([
            'reminder_id' => 'required|integer',
        ]);

        $users = request()->user();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        $reminder = Reminder::query()->findOrFail($request['reminder_id']);

        $reminderCompartments = ReminderCompartment::query()
            ->where('user_id', '=', $userId)
            ->where('reminder_id', '=', $reminder->id)
            ->save();

        $compartmentPrescriptions = [];
        foreach ($reminderCompartments as $reminderCompartment) {
            $compartmentPrescriptions[] = CompartmentPrescription::query()
                ->where('user', '=', $userId)
                ->where('compartment_id', '=', $reminderCompartment['compartment_id'])
                ->save();
        }

        $prescriptions = Prescription::query()
            ->where('user_id', '=', $userId)
            ->save();

        $compartments = Compartment::query()
            ->where('user_id', '=', $userId)
            ->get();

        $response = [];
        foreach ($compartmentPrescriptions as $key => $compartmentPrescription) {
            foreach ($compartmentPrescription as $prescription) {
                $response[$key]['compartment'] = $compartments->where('id', '=', $prescription['compartment_id'])->first();
                $response[$key]['prescription'][] = $prescriptions->where('id', '=', $prescription['prescription_id'])->first();
            }
            $response[$key]['is_snooze_allowed'] = $response[$key]['prescription'][0]['is_snooze_allowed'];
        }
        return $response;
    }
}
