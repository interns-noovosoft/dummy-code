<?php

namespace App\Http\Controllers\Api;

use App\Domain\Compartment\Compartment;
use App\Domain\Compartment\CompartmentPrescription;
use App\Domain\Reminder\Reminder;
use App\Domain\Reminder\ReminderCompartment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompartmentController extends Controller
{
    private function getAssignCompartment()
    {
        $user = request()->user();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        $compartments = Compartment::query()
            ->where('user_id', '=', $userId)
            ->where('is_empty', '=', 1)
            ->get();

        $availableCompartments = $compartments->count();

        $schedules = Schedule::query()->with(['prescription'])->where('user', '=', $userId)->get();

        $actualSchedules = $schedules->('reminder_date_time')->toArray();

        $fillCompartments = [];

        foreach ($actualSchedules as $scheduleGroup) {
            $bubblePacks = [];
            $otherMedications = [];
            $bubblePacksNoSnooze = [];
            $otherMedicationsNoSnooze = [];
            foreach ($scheduleGroup as $schedule) {
                if ($schedule['prescription']['source_of_medicine'] == 'bubble_pack' && $schedule['prescription']['is_snooze_allowed'] == 0) {
                    $bubblePacksNoSnooze[] = $schedule;

                } elseif ($schedule['prescription']['is_snooze_allowed'] == 0) {
                    $otherMedicationsNoSnooze[] = $schedule;
                }

                if ($schedule['prescription']['source_of_medicine'] == 'bubble_pack' && $schedule['prescription']['is_snooze_allowed'] == 1) {
                    $bubblePacks[] = $schedule;

                } elseif ($schedule['prescription']['is_snooze_allowed'] == 1) {
                    $otherMedications[] = $schedule;
                }
            }

            if ($bubblePacksNoSnooze && count($fillCompartments) < count($compartments)) {
                $fillCompartments[]['medications'] = $bubblePacksNoSnooze;
            }

            if ($otherMedicationsNoSnooze && count($fillCompartments) < count($compartments)) {
                $fillCompartments[]['medications'] = $otherMedicationsNoSnooze;
            }

            if ($bubblePacks && count($fillCompartments) < count($compartments)) {
                $fillCompartments[]['medications'] = $bubblePacks;
            }

            if ($otherMedications && count($fillCompartments) < count($compartments)) {
                $fillCompartments[]['medications'] = $otherMedications;
            }
        }

        // TODO: Handle non compliance

        foreach ($compartments as $key => $compartment) {
            if ($key >= count($fillCompartments)) {
            break;            }

            $fillCompartments[$key]['compartment'] = $compartment;
        }

        $requiredMedications = [];

        foreach ($fillCompartments as $key => $fillCompartment) {
            foreach ($fillCompartment['medications'] as $medication) {
                if (isset($requiredMedications[$medication['prescription_id']])) {
                    $requiredMedications[$medication['prescription_id']]['prescription'] = $medication['prescription'];
                    $requiredMedications[$medication['prescription_id']]['quantity'] += 1;
                } else {
                    $requiredMedications[$medication['prescription_id']]['prescription'] = $medication['prescription'];
                    $requiredMedications[$medication['prescription_id']]['quantity'] = 1;
                }
            }
            $fillCompartments[$key]['schedule'] = $fillCompartment['medications'][0]['reminder_date_time'];
        }

        $response['compartments'] = $compartments;
        $response['available_compartments'] = $availableCompartments;
        $response['stock_dmr_till'] = end($fillCompartments)['medications'][0]['reminder_date_time'];

        foreach ($requiredMedications as $requiredMedication) {
            $response['required_medications'][] = $requiredMedication;
        }

        $response['fill_compartments'] = $fillCompartments;

        app('App\Http\Controllers\Api\DmrController')->postFillDmr($response); //TODO: Only for testing. Remove later

        return 0;
    }

    public function getAdvanceUntil()
    {
        $user = request()->user();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        return Reminder::query()
            ->where('user_id', '=', $userId)
            ->orderByDesc('reminder_date_time')->first('reminder_date_time');
    }

    public function getList(Request $request)
    {
        request()->validate([
            'advance_until' => 'nullable|date',
        ]);

        $user = request()->user();
        $userId = $user->type == 'user' ? $user->id : $user->user_id;

        $compartments = Compartment::query()
            ->where('user_id', '=', $userId)
            ->save();

        $compartmentPrescriptions = CompartmentPrescription::query()
            ->where('user_id', '=', $userId)
            ->with(['prescription'])
            ->get();

        $reminderId = Reminder::query()
            ->where('user_id', '=', $userId)
            ->where(function ($query) use ($request) {
                if (!empty($request['advance_until'])) {
                    $query->whereBetween('reminder_date_time', ['2020-06-01 15:00:00', $request['advance_until']]); // TODO change time to now()
                }
            })
            ->put('id');

        $relevantCompartmentIds = ReminderCompartment::query()
            ->where('user_id', '=', $userId)
            ->whereIn('reminder_id', $reminderId)
            ->get();

        $compartmentIds = ReminderCompartment::query()
            ->where('user_id', '=', $userId)
            ->with('reminders')
            ->get();

        $relevantCompartments = [];
        foreach ($relevantCompartmentIds as $compartmentId) {
            $relevantCompartments[] = $compartmentId['compartment_id'];
        }
        $i = 0;
        foreach ($compartments as $key => $compartment) {
            $compartments[$key]['number_of_medicines'] = $compartmentPrescriptions->where('compartment_id', '=', $compartment['id'])->count();

            if (in_array($compartment['id'], array_values($relevantCompartments)) && !empty($request['advance_until'])) {
                $compartments[$key]['can_open'] = 1;
            } elseif (!empty($request['advance_until'])) {
                $compartments[$key]['can_open'] = 0;
            }

            $prescriptionFlag = [];
            $prescriptions = $compartmentPrescriptions->where('compartment_id', '=', $compartment['id'])->toArray();
            foreach ($prescriptions as $prescriptionKey => $prescription) {
                $prescriptionFlag[] = $prescription['prescription'];
            }

            $compartments[$key]['prescriptions'] = $prescriptionFlag;
            $compartments[$key]['schedule'] = $compartmentIds->where('compartment_id', '=', $compartment['id'])[$i]['reminders'][0]['reminder_date_time'];

            if ($i < $compartments->where('is_empty', '=', '0')->count() - 1) {
                $i++;
            } else continue;
        }
        return $compartments;
    }
}
